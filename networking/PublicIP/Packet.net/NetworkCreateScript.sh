#!/bin/sh
export PATH=$PATH:/usr/local/bin/:/usr/local/sbin/
VM_UUID=$1
VM_IP=$2
PACKET_TOKEN=`cat /usr/local/etc/fifo.cloud/packet.key`

NIC_TAG=`vmadm get $VM_UUID | jq -r '.nics | .[] | select(.ip=="'$VM_IP'") | .nic_tag'`
GZ_IF_NAME=`awk -F "=" '/'$NIC_TAG'/ {print $2}' /usr/local/etc/vmadm.toml | tr -d ' ' | tr -d '"'`
JAIL_IF_NAME=`vmadm get $VM_UUID | jq -r '.nics | .[] | select(.ip=="'$VM_IP'") | .interface'`

# Associate IP with hypervisor
echo '{"address":"'$VM_IP'/32","manageable":"false"}' | curl -H 'X-Auth-Token: '$PACKET_TOKEN -d @- -H "Content-Type: application/json" https://api.packet.net/devices/`curl -s https://metadata.packet.net/metadata | jq -r .id`/ips

# append string to file if missing
# usage: appendifmissing <file> <string>
appendifmissing ()
{
    grep -q -F "${2}" "${1}" || echo "${2}" >> "${1}"
}

# Setup Routing to VM
appendifmissing /usr/local/etc/fifo.cloud/global.routes "route add ${VM_IP} -iface $GZ_IF_NAME"
route add ${VM_IP} -iface $GZ_IF_NAME

#Setup Routing from VM
echo '{"set_routes": {"192.168.255.255": "'$JAIL_IF_NAME'"}}' | vmadm update $VM_UUID
