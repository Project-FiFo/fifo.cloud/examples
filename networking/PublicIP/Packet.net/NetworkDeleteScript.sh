#!/bin/sh
set -x
export PATH=$PATH:/usr/local/bin/:/usr/local/sbin/
VM_UUID=$1
VM_IP=$2
PACKET_TOKEN=`cat /usr/local/etc/fifo.cloud/packet.key`
ESCAPED_IP=`echo $VM_IP | sed 's/\./\\\\./g'`

sed -i "" '/'$ESCAPED_IP'/d' /usr/local/etc/fifo.cloud/global.routes
route delete $VM_IP

DEVICE_ID=`curl -s https://metadata.packet.net/metadata | jq -r .id`
IP_ID=`curl -X GET -s -H 'X-Auth-Token: '$PACKET_TOKEN https://api.packet.net/devices/$DEVICE_ID | jq -r '.ip_addresses |.[] | select(.address=="'$VM_IP'")| .id'`
curl -X DELETE -H 'X-Auth-Token: '$PACKET_TOKEN https://api.packet.net/ips/$IP_ID
