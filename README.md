# FiFo.Cloud Examples

This repo is used for holding example script to be used with Fifo.Cloud.
Files hosted here probably dont apply to on-prem fifo.

Have something cool that others might find useful? Share it here! PR's gladly accepted.

No warranty or guarantee on anything here. Questions? Hit us up on freenode #project-fifo.
